//
// Created by shraw on 2/9/2023.
//

#ifndef HACK_ASSEMBLER_C_SYMBOLTABLE_H
#define HACK_ASSEMBLER_C_SYMBOLTABLE_H

#define     PREFILL_SYMBOL  24
#define     NEXT_ADDR       16
#define     ADDR_INC        1
#define     STR_SIZE        16

#include    <cstdlib>
#include    <cstring>
#include    <stdexcept>
#include    <iostream>

using namespace std;

typedef struct{
    char*   symbol;
    int     address;
}   Symbol;

class   SymbolTable{

    private:

        Symbol* symbolTable;
        int     symbolCount;
        int     nextAddress;

    public:

        SymbolTable();
        ~SymbolTable();

        Symbol*    appendSymbol    (char* s);

        int     searchAddress(const char *symbol) {
            for(int i = 0; i < symbolCount; i++){
                if(!strcmp(symbol,symbolTable[i].symbol)){
                    return  symbolTable[i].address;
                }
            }
            return -1;
        }

    [[maybe_unused]] char*   searchSymbol(int address) {
            for(int i = 0; i < symbolCount; i++){
                if(address  ==  symbolTable[i].address){
                    return  symbolTable[i].symbol;
                }
            }
            return nullptr;
        }

};


#endif //HACK_ASSEMBLER_C_SYMBOLTABLE_H
