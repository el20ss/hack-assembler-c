//
// Created by shraw on 2/10/2023.
//

#ifndef HACK_ASSEMBLER_C_CODEGENERATOR_H
#define HACK_ASSEMBLER_C_CODEGENERATOR_H

#define     INS_SIZE    16

#include    <stdexcept>
#include    <cstdio>
#include    <cstring>
#include    <cstdlib>


using namespace std;

class   CodeGenerator{

    private:
        FILE*   writefile;

    public:

    explicit CodeGenerator(char* fname);
    ~CodeGenerator();

    int     writeAIns   (int data, char* out);
    int     writeCIns   (const char* dest, const char* comp, const char* jump, char* out);

};

#endif //HACK_ASSEMBLER_C_CODEGENERATOR_H
