//
// Created by shraw on 2/11/2023.
//

#ifndef HACK_ASSEMBLER_C_ASSEMBLER_H
#define HACK_ASSEMBLER_C_ASSEMBLER_H

#define     REG_COUNT   8
#define     REG_SIZE    5

#define     JMP_COUNT   8
#define     JMP_SIZE    5

#define     COMP_COUNT   10
#define     COMP_SIZE    1

#include    "Lexer.hpp"
#include    "SymbolTable.hpp"
#include    "CodeGenerator.hpp"
#include    <iostream>


const   static   char* registers [] =   {
        "D","M","A","null",
        "MD","AM","AD","AMD"
};

const   char* jumpIns   []  =   {
        "null","JGT","JEQ","JGE","JLT",
        "JNE","JLE","JMP"
};

const   char* compIns   []=   {
        "0","1","D","A","M",
        "+","-","&","|","!"
};

class Assembler{

private:
    Lexer*  lexer;
    SymbolTable*    symboltable;
    CodeGenerator*  codegen;

public:
    explicit Assembler(char* fname);
    ~Assembler();

    int     parse();
    int     parse_first();
    int     parse_second();

    int     is_A_ins();
    int     is_C_ins();

    static int     isRegister(char* reg){

        for(auto & i : registers){
            if(!strcmp(reg,i)){
                return 0;
            }
        }
        return 1;
    }

    static int     isComputation(char* comp){

        for(auto & compIn : compIns){
            if(!strcmp(comp,compIn)){
                return 0;
            }
        }
        return 1;
    }

    static int     isJump(char* jump){

        for(auto & jumpIn : jumpIns){
            if(!strcmp(jump,jumpIn)){
                return 0;
            }
        }
        return 1;
    }

};


#endif //HACK_ASSEMBLER_C_ASSEMBLER_H
