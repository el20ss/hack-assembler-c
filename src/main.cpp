//
// Created by shraw on 2/15/2023.
//

#include    "Lexer.hpp"

int main(int argc, char* argv[]){

    Lexer*  lex =   new Lexer(argv[1]);

    lex->tokenize();


    return 0;
}