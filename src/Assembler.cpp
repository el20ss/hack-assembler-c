//
// Created by shraw on 2/11/2023.
//

#include "Assembler.hpp"



Assembler::Assembler(char* fname){

    try{
        this->lexer         =   new Lexer(fname);
        this->symboltable   =   new SymbolTable();
        this->codegen       =   new CodeGenerator(fname);
    }   catch(const char*   err){
        cout << "Failed to create object : Assembler\n"
        << "Caused by :" << err << endl;
    }
}

Assembler::~Assembler(){

    free(this->lexer);
    free(this->symboltable);
    free(this->codegen);
}

int Assembler::parse(){

    this->lexer->tokenize();

    if(parse_first()){
        //Error has occurred
        return 1;
    }

    if(parse_second()){
        //Error has occurred
        return 1;
    }

    return 0;
}

int Assembler::parse_first(){

    Token t =   this->lexer->peekNextToken();

    while(t.type != EOF){
        t   =   this->lexer->getNextToken();

        if( t.type == OPERATOR &&   !strcmp(t.string,"(")){
            t   =   this->lexer->getNextToken();
            if(t.type == SYMBOL){
                this->symboltable->appendSymbol(t.string);
            }
        }

        if(t.type == ERROR){
            return 1;
        }
    }

    this->lexer->resetTokens();

    return 0;

}

int Assembler::parse_second(){

    Token   t   =   this->lexer->peekNextToken();

    while(t.type != EOF){

        if(!strcmp(t.string,"@")){
            t   = this->lexer->getNextToken();
            //A-instruction
            if(is_A_ins()){
                return 1;
            }
        }
        else{
            //C-instruction
            if(is_C_ins()){
                return 1;
            }
        }
        t   =   this->lexer->peekNextToken();
    }

    return 0;
}

int Assembler::is_A_ins(){
    //@value/@symbol
    Token   t   =   this->lexer->getNextToken();

    if(t.type == VALUE){
        this->codegen->writeAIns((int) strtol(t.string, nullptr,10), nullptr);

    }else if(t.type == SYMBOL){
        //@variable || @label
        if(this->symboltable->searchAddress(t.string) != -1){
            this->codegen->writeAIns(this->symboltable->searchAddress(t.string), nullptr);
        }else{
            this->symboltable->appendSymbol(t.string);
            this->codegen->writeAIns(this->symboltable->searchAddress(t.string), nullptr);
        }

    }else{
        //error
        return 1;
    }
    return 0;
}

int Assembler::is_C_ins(){

    //dest = comp;jmp (dest and jmp can be omitted)
    Token   t       =   this->lexer->peekNextToken();
    char*   dest    =   nullptr;
    char*   comp    =   (char*) calloc(STR_SIZE,sizeof(char));
    char*   jump    =   nullptr;

    if(!isRegister(t.string)) {
        t   =   this->lexer->getNextToken();
        //can be dest or start of comp
        if (this->lexer->peekNextToken().type == OPERATOR
            && !strcmp(this->lexer->peekNextToken().string,"=")) {

            dest    =   (char*) malloc(sizeof(char)*STR_SIZE);
            strcpy(dest,t.string);

            this->lexer->getNextToken();
        }
    }

    t   = this->lexer->peekNextToken();

    while(!isComputation(t.string)){
        t   =   this->lexer->getNextToken();
        strcat(comp,t.string);
        t   =   this->lexer->peekNextToken();
    }

    t   =   this->lexer->peekNextToken();
    if(t.type == OPERATOR && !strcmp(t.string, ";")){
        this->lexer->getNextToken();
        t   = this->lexer->getNextToken();
        if(!isJump(t.string)){
            jump    =   (char*) malloc(sizeof(char)*STR_SIZE);
            strcpy(jump,t.string);
        }   else{
            return 1;
        }
    }

    this->codegen->writeCIns(dest,comp,jump, nullptr);

    if(dest != nullptr){
        free(dest);
    }
    free(comp);
    if(jump != nullptr){
        free(jump);
    }

    return 0;
}
