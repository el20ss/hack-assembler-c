//
// Created by shraw on 2/10/2023.
//

#include "CodeGenerator.hpp"


CodeGenerator::CodeGenerator(char *fname) {

    this->writefile = fopen(fname, "w+");
    char c  = fgetc(writefile);
    if (ferror(writefile)) {
        throw std::invalid_argument("Failed to create object : CodeGenerator");
    }
}

CodeGenerator::~CodeGenerator() {
    fclose(writefile);
}

int CodeGenerator::writeAIns(int data, char* out) {

    char    binary[INS_SIZE+1];

    for(int i = 0; i < INS_SIZE; i++){
        binary[i]   =   '0';
    }

    int x       =   0;
    while(data != 0 && x < 15){
        if(data % 2 != 0){
            binary[INS_SIZE - 1 - x]   =   1 + '0';
        }
        x++;
        data = data >> 1;
    }

    binary[INS_SIZE]    =   '\0';
    for(int i = 0; i < INS_SIZE; i++){
        fprintf(writefile,"%c",binary[i]);
    }
    fprintf(writefile,"\n",NULL);

    if(out != nullptr){
        std::strcpy(out,binary);
    }

    return 0;
}

int CodeGenerator::writeCIns(const  char* dest, const char* comp, const char* jump, char* out) {

    //000x 0000 0000 x000
    char    binary[INS_SIZE+1];

    for(int i = 0; i < INS_SIZE; i++){
        binary[i]   =   '0';
    }

    if(dest == nullptr){
        binary[INS_SIZE - 1 - 3]    =   '0';
        binary[INS_SIZE - 1 - 4]    =   '0';
        binary[INS_SIZE - 1 - 5]    =   '0';
    }
    else if(!strcmp(dest,"M"))  {
        binary[INS_SIZE - 1 - 3]    =   '1';
        binary[INS_SIZE - 1 - 4]    =   '0';
        binary[INS_SIZE - 1 - 5]    =   '0';
    }
    else if(!strcmp(dest,"D"))  {
        binary[INS_SIZE - 1 - 3]    =   '0';
        binary[INS_SIZE - 1 - 4]    =   '1';
        binary[INS_SIZE - 1 - 5]    =   '0';
    }
    else if(!strcmp(dest,"A"))  {
        binary[INS_SIZE - 1 - 3]    =   '0';
        binary[INS_SIZE - 1 - 4]    =   '0';
        binary[INS_SIZE - 1 - 5]    =   '1';
    }
    else if(!strcmp(dest,"MD")) {
        binary[INS_SIZE - 1 - 3]    =   '1';
        binary[INS_SIZE - 1 - 4]    =   '1';
        binary[INS_SIZE - 1 - 5]    =   '0';
    }
    else if(!strcmp(dest,"AD")) {
        binary[INS_SIZE - 1 - 3]    =   '0';
        binary[INS_SIZE - 1 - 4]    =   '1';
        binary[INS_SIZE - 1 - 5]    =   '1';
    }
    else if(!strcmp(dest,"AM")) {
        binary[INS_SIZE - 1 - 3]    =   '1';
        binary[INS_SIZE - 1 - 4]    =   '0';
        binary[INS_SIZE - 1 - 5]    =   '1';
    }
    else if(!strcmp(dest,"AMD")){
        binary[INS_SIZE - 1 - 3]    =   '1';
        binary[INS_SIZE - 1 - 4]    =   '1';
        binary[INS_SIZE - 1 - 5]    =   '1';
    }
    else{
        return 1;
    }

    if(comp == NULL){
        return 1;
    }
    else if(!strcmp(comp,"0"))  {
        binary[INS_SIZE - 1 - 6]        =   '0';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '1';
        binary[INS_SIZE - 1 - 10]       =   '0';
        binary[INS_SIZE - 1 - 11]       =   '1';
    }
    else if(!strcmp(comp,"1"))  {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '1';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '1';
    }
    else if(!strcmp(comp,"-1")) {
        binary[INS_SIZE - 1 - 6]        =   '0';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '1';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '1';
    }
    else if(!strcmp(comp,"D"))  {
        binary[INS_SIZE - 1 - 6]        =   '0';
        binary[INS_SIZE - 1 - 7]        =   '0';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '1';
        binary[INS_SIZE - 1 - 10]       =   '0';
        binary[INS_SIZE - 1 - 11]       =   '0';
    }
    else if(!strcmp(comp,"!D")) {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '0';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '1';
        binary[INS_SIZE - 1 - 10]       =   '0';
        binary[INS_SIZE - 1 - 11]       =   '0';
    }
    else if(!strcmp(comp,"-D")) {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '1';
        binary[INS_SIZE - 1 - 10]       =   '0';
        binary[INS_SIZE - 1 - 11]       =   '0';
    }
    else if(!strcmp(comp,"D+1")){
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '1';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '0';
    }
    else if(!strcmp(comp,"D-1")){
        binary[INS_SIZE - 1 - 6]        =   '0';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '1';
        binary[INS_SIZE - 1 - 10]       =   '0';
        binary[INS_SIZE - 1 - 11]       =   '0';
    }
    else if(!strcmp(comp,"A")   || !strcmp(comp,"M"))   {
        binary[INS_SIZE - 1 - 6]        =   '0';
        binary[INS_SIZE - 1 - 7]        =   '0';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '1';

        if(!strcmp(comp,"M")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"!A")  || !strcmp(comp,"!M"))  {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '0';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '1';

        if(!strcmp(comp,"!M")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"-A")  || !strcmp(comp,"-M"))  {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '1';

        if(!strcmp(comp,"-M")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"A+1") || !strcmp(comp,"M+1")) {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '1';

        if(!strcmp(comp,"M+1")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"A-1") || !strcmp(comp,"M-1")) {
        binary[INS_SIZE - 1 - 6]        =   '0';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '1';

        if(!strcmp(comp,"M-1")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"D+A") || !strcmp(comp,"D+M")) {
        binary[INS_SIZE - 1 - 6]        =   '0';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '0';
        binary[INS_SIZE - 1 - 11]       =   '0';

        if(!strcmp(comp,"D+M")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"D-A") || !strcmp(comp,"D-M")) {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '0';

        if(!strcmp(comp,"D-M")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"A-D") || !strcmp(comp,"M-D")) {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '1';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '0';
        binary[INS_SIZE - 1 - 11]       =   '0';

        if(!strcmp(comp,"M-D")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"D&A") || !strcmp(comp,"D&M")) {
        binary[INS_SIZE - 1 - 6]        =   '0';
        binary[INS_SIZE - 1 - 7]        =   '0';
        binary[INS_SIZE - 1 - 8]        =   '0';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '0';
        binary[INS_SIZE - 1 - 11]       =   '0';

        if(!strcmp(comp,"D&M")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else if(!strcmp(comp,"D|A") || !strcmp(comp,"D|M")) {
        binary[INS_SIZE - 1 - 6]        =   '1';
        binary[INS_SIZE - 1 - 7]        =   '0';
        binary[INS_SIZE - 1 - 8]        =   '1';
        binary[INS_SIZE - 1 - 9]        =   '0';
        binary[INS_SIZE - 1 - 10]       =   '1';
        binary[INS_SIZE - 1 - 11]       =   '0';

        if(!strcmp(comp,"D|M")){
            binary[INS_SIZE - 1 - 12]       =   '1';
        }
    }
    else{
        return 1;
    }

    if(jump == NULL){
        binary[INS_SIZE - 1 - 0]    =   '0';
        binary[INS_SIZE - 1 - 1]    =   '0';
        binary[INS_SIZE - 1 - 2]    =   '0';
    }
    else if(!strcmp(jump,"JGT"))  {
        binary[INS_SIZE - 1 - 0]    =   '1';
        binary[INS_SIZE - 1 - 1]    =   '0';
        binary[INS_SIZE - 1 - 2]    =   '0';
    }
    else if(!strcmp(jump,"JEQ"))  {
        binary[INS_SIZE - 1 - 0]    =   '0';
        binary[INS_SIZE - 1 - 1]    =   '1';
        binary[INS_SIZE - 1 - 2]    =   '0';
    }
    else if(!strcmp(jump,"JGE"))  {
        binary[INS_SIZE - 1 - 0]    =   '1';
        binary[INS_SIZE - 1 - 1]    =   '1';
        binary[INS_SIZE - 1 - 2]    =   '0';
    }
    else if(!strcmp(jump,"JLT"))  {
        binary[INS_SIZE - 1 - 0]    =   '0';
        binary[INS_SIZE - 1 - 1]    =   '0';
        binary[INS_SIZE - 1 - 2]    =   '1';
    }
    else if(!strcmp(jump,"JNE"))  {
        binary[INS_SIZE - 1 - 0]    =   '1';
        binary[INS_SIZE - 1 - 1]    =   '0';
        binary[INS_SIZE - 1 - 2]    =   '1';
    }
    else if(!strcmp(jump,"JLE"))  {
        binary[INS_SIZE - 1 - 0]    =   '0';
        binary[INS_SIZE - 1 - 1]    =   '1';
        binary[INS_SIZE - 1 - 2]    =   '1';
    }
    else if(!strcmp(jump,"JMP"))  {
        binary[INS_SIZE - 1 - 0]    =   '1';
        binary[INS_SIZE - 1 - 1]    =   '1';
        binary[INS_SIZE - 1 - 2]    =   '1';
    }
    else{
        return 1;
    }

    binary[INS_SIZE - 1 - 13]       =   '1';
    binary[INS_SIZE - 1 - 14]       =   '1';
    binary[INS_SIZE - 1 - 15]       =   '1';
    binary[INS_SIZE]    =   '\0';

    for(int i = 0; i < INS_SIZE; i++){
        fprintf(writefile,"%c",binary[i]);
    }
    fprintf(writefile,"\n",NULL);

    if(out != nullptr){
        std::strcpy(out,binary);
    }

    return 0;
}

