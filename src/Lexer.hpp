//
// Created by shraw on 2/5/2023.
//

#ifndef HACK_ASSEMBLER_C_LEXER_H
#define HACK_ASSEMBLER_C_LEXER_H

#define STR_SIZE 16

#include    <cstring>
#include    <cstdlib>
#include    <cstdio>
#include    <cctype>
#include    <stdexcept>
#include    <iostream>

using namespace std;

typedef enum{
    VALUE,
    OPERATOR,
    SYMBOL,
    ERROR
}   LexerConstants;

typedef struct{
    char*   string;
    int     type;
}   Token;

const char operators[] =   {
        '[',']','=',';','(',')','@','+','\\','-','&','|','!','\0'
};

class   Lexer{

    private:
        char*   filename;
        char*   filepath;

        Token*  tokens;
        int     tokenCount;
        int     nextToken;

        static int     isOperator  (const char* c) {
            int i   =   0;
            while(operators[i] != '\0'){
                if(*c == operators[i]){
                    return 0;
                }
                i++;
            }
            return 1;
        }

        static int     isNumeric   (const char* c) {

            if(isdigit(*c)){
                return 0;
            }
            return 1;
        }

        static int     isSymbol    (const char* c) {

            if(isalpha(*c)){
                return 0;
            }
            return 1;
        }

    public:
        explicit    Lexer   (char* fname);
        ~Lexer();

        int     tokenize();
        int     appendToken(Token t);

        char*   getFilename()   {
            return this->filename;
        }

        char*   getFilepath()   {
            return this->filepath;
        }

        Token   getNextToken()  {
            return tokens[nextToken++];
        }

        Token   peekNextToken() {
            return tokens[nextToken];
        }

        void    resetTokens()   {
            this->nextToken =   0;
        }

};



#endif //HACK_ASSEMBLER_C_LEXER_H
