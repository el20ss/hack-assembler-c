//
// Created by shraw on 2/13/2023.
//
#include    <gtest/gtest.h>
#include    <fstream>
#include    <stdexcept>
#include    "../src/Lexer.hpp"

#define TEST_VALS   6
const   char*   lex_test_symbols[]      =   {"D" , "=" , "D" , "+" , "A" , "@", "360"};
const   LexerConstants test_const[] =   {SYMBOL,OPERATOR,SYMBOL,OPERATOR,SYMBOL,OPERATOR,VALUE};

TEST(HACK_Assembler_Tests, Lex_constructor_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,R"(C:\Users\shraw\Downloads\assembly\Add.asm)");

    Lexer*  test_lexer;
    try {
        test_lexer  = new Lexer(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Lexer";
    }

    ASSERT_STREQ(test_lexer->getFilename(),tmp);

    free(tmp);
    free(test_lexer);

    SUCCEED();
}

TEST(HACK_Assembler_Tests, tokenize_test)   {

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,R"(C:\Users\shraw\Downloads\assembly\Add.asm)");

    Lexer*  test_lexer;
    try {
        test_lexer  = new Lexer(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Lexer";
    }

    test_lexer->tokenize();

    string  line;
    ifstream readTokens(R"(C:\Users\shraw\Downloads\assembly\Add.tokens)");

    while(test_lexer->peekNextToken().type != EOF && std::strcmp(line.c_str(),"-1") != 0){

        if(!getline(readTokens,line)){
            FAIL() << "Invalid number of tokens at token : " << test_lexer->peekNextToken().string;
        }
        ASSERT_STREQ(line.c_str(),test_lexer->peekNextToken().string);

        if(!getline(readTokens,line)) {
            FAIL() << "Invalid number of tokens" << test_lexer->peekNextToken().string;
        }
        ASSERT_EQ(stoi(line),test_lexer->getNextToken().type);
    }

    readTokens.close();

    free(tmp);
    free(test_lexer);

    SUCCEED();

}

TEST(HACK_Assembler_Tests, appendToken_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,R"(C:\Users\shraw\Downloads\assembly\Empty.asm)");

    Lexer*  test_lexer;
    try {
        test_lexer  = new Lexer(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Lexer";
    }

    Token t_tmp;
    t_tmp.string    =   (char*) malloc(sizeof(char)*8);
    for(int i = 0; i < TEST_VALS; i++){
        std::strcpy(t_tmp.string,lex_test_symbols[i]);
        t_tmp.type  =   test_const[i];
        test_lexer->appendToken(t_tmp);
    }

    for(int i = 0; i < TEST_VALS; i++){
        ASSERT_STREQ(test_lexer->peekNextToken().string, lex_test_symbols[i]);
        ASSERT_EQ(test_lexer->getNextToken().type, test_const[i]);
    }

    free(t_tmp.string);
    SUCCEED();
}

TEST(HACK_Assembler_Tests, peekNextToken_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,R"(C:\Users\shraw\Downloads\assembly\Empty.asm)");

    Lexer*  test_lexer;
    try {
        test_lexer  = new Lexer(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Lexer";
    }

    Token t_tmp;
    t_tmp.string    =   (char*) malloc(sizeof(char)*8);
    for(int i = 0; i < TEST_VALS; i++){
        std::strcpy(t_tmp.string,lex_test_symbols[i]);
        t_tmp.type  =   test_const[i];
        test_lexer->appendToken(t_tmp);
    }

    for(int i = 0; i < TEST_VALS; i++){
        ASSERT_STREQ(test_lexer->peekNextToken().string, lex_test_symbols[0]);
        ASSERT_EQ(test_lexer->peekNextToken().type, test_const[0]);
    }

    free(t_tmp.string);
    SUCCEED();

}

TEST(HACK_Assembler_Tests, getNextToken_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,R"(C:\Users\shraw\Downloads\assembly\Empty.asm)");

    Lexer*  test_lexer;
    try {
        test_lexer  = new Lexer(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Lexer";
    }

    Token t_tmp;
    t_tmp.string    =   (char*) malloc(sizeof(char)*8);
    for(int i = 0; i < TEST_VALS; i++){
        std::strcpy(t_tmp.string,lex_test_symbols[i]);
        t_tmp.type  =   test_const[i];
        test_lexer->appendToken(t_tmp);
    }

    for(int i = 0; i < TEST_VALS; i++){
        EXPECT_STREQ(test_lexer->peekNextToken().string, lex_test_symbols[i]);
        ASSERT_EQ(test_lexer->getNextToken().type, test_const[i]);
    }

    free(t_tmp.string);
    SUCCEED();

}