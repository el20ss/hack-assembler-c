//
// Created by shraw on 2/20/2023.
//

#include    <gtest/gtest.h>
#include    <stdexcept>
#include    "../src/SymbolTable.hpp"

#define TEST_VALS   6

const   char*   sym_test_symbols[]      =   {"A" , "B" , "C" , "ABC" , "CDE" , "FGH"};
const   char*   test_constructor[]  =   {"R0","R1","R2","R3","R4","R5",
                                         "R6","R7","R8","R9","R10","R11",
                                         "R12","R13","R14","R15","SP","LCL",
                                         "ARG","THIS","THAT","SCREEN", "KBO" };

TEST(HACK_Assembler_Tests, SymbolTable_constructor_test){

    SymbolTable*    symbolTable;

    try {
        symbolTable  = new SymbolTable();
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : SymbolTable";
    }

    ASSERT_EQ(symbolTable->searchAddress(test_constructor[0])   ,0);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[1])   ,1);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[2])   ,2);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[3])   ,3);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[4])   ,4);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[5])   ,5);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[6])   ,6);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[7])   ,7);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[8])   ,8);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[9])   ,9);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[10])  ,10);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[11])  ,11);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[12])  ,12);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[13])  ,13);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[14])  ,14);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[15])  ,15);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[16])  ,0);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[17])  ,1);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[18])  ,2);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[19])  ,3);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[20])  ,4);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[21])  ,16384);
    ASSERT_EQ(symbolTable->searchAddress(test_constructor[22])  ,24576);

    SUCCEED();
}

TEST(HACK_Assembler_Tests, appendSymbol_test){

    SymbolTable*    symbolTable;
    try {
        symbolTable  = new SymbolTable();
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : SymbolTable";
    }

    for(auto & test_symbol : sym_test_symbols){
        ASSERT_NE(symbolTable->appendSymbol(const_cast<char *>(test_symbol)) , nullptr);
    }

    ASSERT_EQ(symbolTable->searchAddress(sym_test_symbols[0])  ,16);
    ASSERT_EQ(symbolTable->searchAddress(sym_test_symbols[1])  ,17);
    ASSERT_EQ(symbolTable->searchAddress(sym_test_symbols[2])  ,18);
    ASSERT_EQ(symbolTable->searchAddress(sym_test_symbols[3])  ,19);
    ASSERT_EQ(symbolTable->searchAddress(sym_test_symbols[4])  ,20);
    ASSERT_EQ(symbolTable->searchAddress(sym_test_symbols[5])  ,21);

    SUCCEED();

}