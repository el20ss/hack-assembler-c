//
// Created by shraw on 2/22/2023.
//
#include    <gtest/gtest.h>
#include    <fstream>
#include    <stdexcept>
#include    "../src/Assembler.hpp"

#define TEST_VALS   6
const   char*   assembler_AIns_[]  =   {"@35", "@TEST", "@75315", nullptr};
const   char*   assembler_CIns_[]  =   {};



TEST(HACK_Assembler_Tests, Assembler_constructor_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,R"(C:\Users\shraw\Downloads\assembly\Add.asm)");

    Assembler*  test_assembler;
    try {
        test_assembler  = new Assembler(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Assembler";
    }

    free(tmp);
    free(test_assembler);

    SUCCEED();
}

TEST(HACK_Assembler_Tests, isAIns_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,R"(C:\Users\shraw\Downloads\assembly\Add.asm)");

    Assembler*  test_assembler;
    try {
        test_assembler  = new Assembler(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Assembler";
    }

    free(tmp);
    free(test_assembler);

    SUCCEED();

}

TEST(HACK_Assembler_Tests, isCIns_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,R"(C:\Users\shraw\Downloads\assembly\Add.asm)");

    Assembler*  test_assembler;
    try {
        test_assembler  = new Assembler(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Assembler";
    }

    free(tmp);
    free(test_assembler);

    SUCCEED();


}

