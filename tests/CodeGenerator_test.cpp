//
// Created by shraw on 2/20/2023.
//
#include    <gtest/gtest.h>
#include    <stdexcept>
#include    <fstream>
#include    "../src/CodeGenerator.hpp"

#define TEST_VALS   6

const   char*   cg_dest_symbols[]   =   {"D" , "AD" ,"AMD","MD" ,"A",nullptr};
const   char*   cg_comp_symbols[]   =   {"0" , "!D" ,"-M" ,"A-1","D+M","D|A"};
const   char*   cg_jump_symbols[]   =   {"JGT","JGE","JNE","JMP","JEQ",nullptr};

int     cg_aIns_symbols[]           =   {1,23,456,7890,2,34};

TEST(HACK_Assembler_Tests, CodeGenerator_constructor_test){
    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,"Add.txt");

    CodeGenerator*  codegen;
    try {
        codegen  = new CodeGenerator(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Lexer";
    }

    free(tmp);
    free(codegen);

    SUCCEED();
}

TEST(HACK_Assembler_Tests, writeAIns_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,"Add.txt");

    CodeGenerator*  codegen;
    try {
        codegen  = new CodeGenerator(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Lexer";
    }

    char*   test    =   (char*) malloc(sizeof(char)*16);

    codegen->writeAIns(cg_aIns_symbols[0],test);
    ASSERT_STREQ(test,"0000000000000001");
    codegen->writeAIns(cg_aIns_symbols[1],test);
    ASSERT_STREQ(test,"0000000000010111");
    codegen->writeAIns(cg_aIns_symbols[2],test);
    ASSERT_STREQ(test,"0000000111001000");
    codegen->writeAIns(cg_aIns_symbols[3],test);
    ASSERT_STREQ(test,"0001111011010010");
    codegen->writeAIns(cg_aIns_symbols[4],test);
    ASSERT_STREQ(test,"0000000000000010");
    codegen->writeAIns(cg_aIns_symbols[5],test);
    ASSERT_STREQ(test,"0000000000100010");

    free(test);
    free(codegen);

    SUCCEED();

}

TEST(HACK_Assembler_Tests, writeCIns_test){

    char*   tmp =   (char*) calloc(64,sizeof(char));
    strcpy(tmp,"Add.txt");
    CodeGenerator*  codegen;
    try {
        codegen  = new CodeGenerator(tmp);
    }   catch (std::invalid_argument& e){
        FAIL() << "Failed to create object : Lexer";
    }

    char*   test    =   (char*) malloc(sizeof(char)*16);

    codegen->writeCIns(cg_dest_symbols[0],cg_comp_symbols[0],cg_jump_symbols[0],test);
    ASSERT_STREQ(test,"1110101010010001");
    codegen->writeCIns(cg_dest_symbols[1],cg_comp_symbols[1],cg_jump_symbols[1],test);
    ASSERT_STREQ(test,"1110001101110011");
    codegen->writeCIns(cg_dest_symbols[2],cg_comp_symbols[2],cg_jump_symbols[2],test);
    ASSERT_STREQ(test,"1111110011111101");
    codegen->writeCIns(cg_dest_symbols[3],cg_comp_symbols[3],cg_jump_symbols[3],test);
    ASSERT_STREQ(test,"1110110010011111");
    codegen->writeCIns(cg_dest_symbols[4],cg_comp_symbols[4],cg_jump_symbols[4],test);
    ASSERT_STREQ(test,"1111000010100010");
    codegen->writeCIns(cg_dest_symbols[5],cg_comp_symbols[5],cg_jump_symbols[5],test);
    ASSERT_STREQ(test,"1110010101000000");

    free(test);
    free(codegen);

    SUCCEED();
}